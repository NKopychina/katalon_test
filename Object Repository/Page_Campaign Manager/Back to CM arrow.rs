<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Back to CM arrow</name>
   <tag></tag>
   <elementGuidId>4dd40360-d920-437a-8f1e-e6bbb021c3eb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-eject</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;wrap&quot;]/div[@class=&quot;page-body&quot;]/aside[@class=&quot;aside-menu&quot;]/div[@class=&quot;client-brand&quot;]/div[@class=&quot;client-brand__current&quot;]/a[@class=&quot;client-brand__eject&quot;]/span[@class=&quot;glyphicon glyphicon-eject&quot;]</value>
   </webElementProperties>
</WebElementEntity>
