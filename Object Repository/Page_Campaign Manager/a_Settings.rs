<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Settings</name>
   <tag></tag>
   <elementGuidId>32a6078b-9035-443b-96c0-25df6bb88961</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[ends-with(@href, '/settings') and (text() = 'Settings' or . = 'Settings')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>ends with</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/settings</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Settings</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;wrap&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;page-content js-page-container&quot;]/div[@class=&quot;js-nav-tabs-region-container&quot;]/div[1]/div[@class=&quot;wrap-tabs&quot;]/ul[@class=&quot;nav-tabs nav&quot;]/li[2]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
