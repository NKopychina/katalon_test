<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_advertiser_name</name>
   <tag></tag>
   <elementGuidId>d2fe3646-f3a5-4935-a03f-6fe0bcbd7c34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'advertiser_name' or . = 'advertiser_name')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/overview</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>link-Name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>advertiser_name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;wrap&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;page-content js-page-container&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;well&quot;]/div[@class=&quot;table-responsive advertisers-list&quot;]/table[@class=&quot;backgrid table table-hover&quot;]/tbody[1]/tr[1]/td[@class=&quot;link-cell sortable renderable Name&quot;]/a[@class=&quot;link-Name&quot;]</value>
   </webElementProperties>
</WebElementEntity>
