<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Ad Groups</name>
   <tag></tag>
   <elementGuidId>93f3f025-49d7-4986-bc4f-972df401edc2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[starts-with(@href, '/campaign/
') and (text() = 'Ad Groups' or . = 'Ad Groups')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[2]/div/div[3]/div/div/ul/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/campaign/
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ad Groups</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;wrap&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;page-content js-page-container&quot;]/div[@class=&quot;js-nav-tabs-region-container&quot;]/div[1]/div[@class=&quot;wrap-tabs&quot;]/ul[@class=&quot;nav-tabs nav&quot;]/li[3]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
