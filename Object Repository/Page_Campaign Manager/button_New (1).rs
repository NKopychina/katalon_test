<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_New (1)</name>
   <tag></tag>
   <elementGuidId>f715d614-07ea-4e9b-8016-20627f31f65b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toolbar-action</name>
      <type>Main</type>
      <value>add</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>                                                    New        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;wrap&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;page-content js-page-container&quot;]/div[@class=&quot;viewport flex&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;well&quot;]/div[@class=&quot;grid-toolbar&quot;]/div[@class=&quot;grid-toolbar-group&quot;]/div[@class=&quot;grid-toolbar-button btn-toolbar&quot;]/div[@class=&quot;btn-group&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
   </webElementProperties>
</WebElementEntity>
