<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Bid Type</name>
   <tag></tag>
   <elementGuidId>c3ba24f8-0222-45c2-a742-9b9b1c18b585</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-group field-BiddingTypeId required' and (contains(text(), 'Bid Type') or contains(., 'Bid Type'))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;c149_BiddingTypeId&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group field-BiddingTypeId required</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bid Type</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>_LifetimeSpend</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>starts with</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>c</value>
   </webElementProperties>
</WebElementEntity>
