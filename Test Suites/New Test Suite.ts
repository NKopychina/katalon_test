<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c5aecf1d-8283-4b78-b3b0-f3a0b2957085</testSuiteGuid>
   <testCaseLink>
      <guid>91e11334-fd39-4764-93b6-8f8fb4d58dce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cases/Create advertiser</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>96a16fa8-dc1d-455b-8da8-ae831fa89709</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f2d5ce33-4980-49ad-8ab2-b77f684895ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cases/Create campaign</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f5f85f91-20a8-4c3e-a0b1-8742ec96a630</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Campaigns</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2fdef6bd-645a-4c75-b3b8-61849c460dc9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ff5a7e10-299b-466a-94a6-1d81116e3f3a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ac377d37-2f76-47f9-b290-d96a325eb03c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
