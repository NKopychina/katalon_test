import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.builtin.GetUrlKeyword as GetUrlKeyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testdata.CSVData as CSVData

CSVData data = findTestData('Campaigns')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://cl-cm-dev.thumbtack.lo/')

WebUI.setText(findTestObject('Page_Campaign Manager/input_email'), 'dzaporozhets@lineate.com')

WebUI.setText(findTestObject('Page_Campaign Manager/input_password'), 'admin')

WebUI.click(findTestObject('Page_Campaign Manager/button_Sign In'))

WebUI.click(findTestObject('Page_Campaign Manager/button_New Advertiser'))

WebUI.setText(findTestObject('Page_Campaign Manager/input_advertiser_name'), advertiser_name)

WebUI.click(findTestObject('Page_Campaign Manager/a_Create'))

WebUI.click(findTestObject('Page_Campaign Manager/a_Campaigns'))

String URL = WebUI.getUrl()

for (def index : (0..data.getRowNumbers() - 1)) {
    WebUI.click(findTestObject('Page_Campaign Manager/button_New'))

    WebUI.selectOptionByValue(findTestObject('Page_Campaign Manager/select_-- Select --Native Prod'), '7', true)

    WebUI.click(findTestObject('Page_Campaign Manager/button_Next'))

    WebUI.setText(findTestObject('Page_Campaign Manager/input_campaign_name'), data.internallyGetValue('campaign_name', 
            index))

    WebUI.setText(findTestObject('Page_Campaign Manager/input_LifetimeBudget'), data.internallyGetValue('budget', index))

    WebUI.click(findTestObject('Page_Campaign Manager/button_Create'))

    WebUI.navigateToUrl(URL)
}

WebUI.click(findTestObject('Page_Campaign Manager/a_Dashboard'))

WebUI.click(findTestObject('Page_Campaign Manager/button_Archive_advertiser'))

WebUI.click(findTestObject('Page_Campaign Manager/a_Yes_archive_advertiser'))

WebUI.closeBrowser()

WebUI.navigateToUrl('')

WebUI.openBrowser('')

WebUI.selectOptionByValue(findTestObject('Page_Campaign Manager (1)/select_-- Select --Native Prod'), '7', true)

